import redis
import psycopg2
import os

postgres_host = os.getenv('POSTGRES_HOST', 'localhost')
redis_host = os.getenv('REDIS_HOST', 'localhost')

r = redis.Redis(host=redis_host, port=6379, password='')

p = psycopg2.connect(
    host=postgres_host,
    database="netbox1",
    user="netbox1",
    password="",
    port=5432
)

print(f'Redis ping: {r.ping()}')
print(f'Postgres ping: {p.closed}')